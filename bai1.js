var numberArr = [];

document.getElementById("btn-so").addEventListener("click", function () {
  var numberValue = document.getElementById("txt-so").value;
  numberArr.push(numberValue);
  document.getElementById("txt-so").value = "";
  document.getElementById("them-so").innerHTML = numberArr;
});
// bài tập 1
document.getElementById("btn-tong").addEventListener("click", function () {
  var sum = 0;
  for (var i = 0; i < numberArr.length; i++) {
    sum += +numberArr[i];
  }

  document.getElementById("them-so1").innerHTML = `Tổng số dương ${sum}`;
});
// bài tập 2
document.getElementById("btn-dem-so").addEventListener("click", function () {
  var sum1 = 0;

  for (var i2 = 0; i2 < numberArr.length; i2++) {
    if (numberArr[i2] > 0) {
      sum1++;
    }
  }
  document.getElementById("them-so2").innerHTML = `Số dương ${sum1}`;
});

// bài tập 3
document.getElementById("btn-nho-nhat").addEventListener("click", function () {
  var sum2 = numberArr[0];

  for (var i3 = 1; i3 < numberArr.length; ++i3) {
    if (sum2 > numberArr[i3]) {
      sum2 = numberArr[i3];
    }
  }
  console.log("số nhỏ nhất", sum2);
  // console.log("fafa", sum22);
  document.getElementById("them-so3").innerHTML = `số nhỏ nhất ${sum2}`;
});

// bài tập 4
document
  .getElementById("btn-so-duong-nho-nhat")
  .addEventListener("click", function () {
    var nun = [];
    for (var j = 0; j < numberArr.length; j++) {
      if (numberArr[j] > 0) {
        nun.push(numberArr[j]);
      }
    }

    var sum3 = nun[0];

    for (var i4 = 1; i4 < nun.length; i4++) {
      if (nun[i4] < sum3) {
        sum3 = nun[i4];
      }
    }
    document.getElementById(
      "them-so4"
    ).innerHTML = `Số dương nhỏ nhất ${sum3} `;
  });

// bài tập 5
document.getElementById("btn-chan-cuoi").addEventListener("click", function () {
  var sum4 = "";
  for (var i5 = 0; i5 < numberArr.length; i5++) {
    if (numberArr[i5] % 2 == 0) {
      sum4 = numberArr[i5];
    }
  }
  document.getElementById("them-so5").innerHTML = `Số chẵn cuối cùng ${sum4}`;
});

// bài tập 6
document.getElementById("btn-doi-cho").addEventListener("click", function () {
  var nunn = [];
  for (var j2 = 0; j2 < numberArr.length; j2++) {
    nunn.push(numberArr[j2]);
  }
  var viTriCaHai = 0;
  var viTri1 = document.getElementById("txt-vi-tri-1").value * 1;
  var viTri2 = document.getElementById("txt-vi-tri-2").value * 1;
  viTriCaHai = nunn[viTri1];
  nunn[viTri1] = nunn[viTri2];
  nunn[viTri2] = viTriCaHai;
  var result = "";
  for (var i6 = 0; i6 < numberArr.length; i6++) {
    result = result.concat(nunn[i6], ",");
  }
  document.getElementById("them-so6").innerHTML = `Mảng sau khi đổi ${result}`;
});

// bài tập 7
document.getElementById("btn-sap-xep").addEventListener("click", function () {
  numberArr.sort();

  var result1 = "";
  for (var i7 = 0; i7 < numberArr.length; i7++) {
    result1 = result1.concat(numberArr[i7], ",");
  }
  document.getElementById(
    "them-so7"
  ).innerHTML = `Mảng sau khi sắp xếp ${result1}`;
});

// bài tập 8
document
  .getElementById("btn-so-nguyen-to")
  .addEventListener("click", function () {
    var soNguyenToDauTien = "";
    var laSNT = true;
    for (var i8 = 0; i8 < numberArr.length; i8++) {
      laSNT = true;
      for (var j3 = 2; j3 < numberArr[i8]; j3++)
        if (numberArr[i8] % j3 == 0) {
          laSNT = false;
          break;
        }
      if (laSNT == true) {
        soNguyenToDauTien = numberArr[i8];
        break;
      }
    }
    document.getElementById(
      "them-so8"
    ).innerHTML = `Số nguyên tố đâu tiên trong mảng  ${soNguyenToDauTien}`;
  });

// bài tập 9
var mangSoThuc = [];
document.getElementById("btn-so-them").addEventListener("click", function () {
  var nhapSoN = document.getElementById("txt-nhap-so-nho").value * 1;
  mangSoThuc.push(nhapSoN);
  var dsPhanTu = "";
  for (var i9 = 0; i9 < mangSoThuc.length; i9++) {
    dsPhanTu = dsPhanTu.concat(mangSoThuc[i9], ", ");
  }
  document.getElementById("them-so9").innerHTML = dsPhanTu;
  document.getElementById("txt-nhap-so-nho").value = "";
});
document
  .getElementById("btn-dem-so-them")
  .addEventListener("click", function () {
    var demSoNguyen = 0;
    for (var i10 = 0; i10 < mangSoThuc.length; i10++) {
      if (Number.isInteger(mangSoThuc[i10])) {
        demSoNguyen++;
      }
    }
    document.getElementById(
      "them-so10"
    ).innerHTML = `Số nguyên trong mảng ${demSoNguyen}`;
  });
// bài tập 10
document.getElementById("btn-so-sanh").addEventListener("click", function () {
  var soAm = 0;
  var soDuong = 0;
  var result2 = "";
  for (var i11 = 0; i11 < numberArr.length; i11++) {
    if (numberArr[i11] < 0) {
      soAm++;
    } else if (numberArr[i11] > 0) {
      soDuong++;
    }
  }
  if (soAm < soDuong) {
    result2 = "Số âm < Số dương";
  } else if (soAm > soDuong) {
    result2 = "Số âm > Số dương";
  } else {
    result2 = "Số âm = Số dương";
  }
  document.getElementById("them-so11").innerHTML = result2;
});
